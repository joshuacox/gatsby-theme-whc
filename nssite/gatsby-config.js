module.exports = {
  plugins: [
    {
      resolve: "@webhostingcoop/gatsby-theme-whc",
      options: {
        contentPath: "events",
        basePath: "/events",
      },
    },
  ],
}

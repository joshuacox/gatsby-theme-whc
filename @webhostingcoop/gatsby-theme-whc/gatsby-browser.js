// Import all js dependencies.
import 'jquery/dist/jquery.min.js'
import 'popper.js/dist/popper.min'
import 'bootstrap/js/dist/util'
import 'bootstrap/js/dist/carousel'
import 'bootstrap/js/dist/dropdown'
import "./src/styles/bootstrap.min.css"
import "./src/styles/bootstrap-theme.css"
//import "./src/styles/normalize.css"
import "./src/styles/styles.css"

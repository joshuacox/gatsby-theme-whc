import React from "react"
import { graphql } from "gatsby"
import Link from 'gatsby-plugin-transition-link'
import AniLink from "gatsby-plugin-transition-link/AniLink"
import Img from "gatsby-image"
import { IconContext } from "react-icons";
import { FaChevronRight } from 'react-icons/fa';
import { FaHeart } from 'react-icons/fa';
import NavBar from '../components/navbar';

export default {
  title: "Dashboard/navbar",
}

export const navbarStory = () => (
  <div>
    <NavBar/>
  </div>
)

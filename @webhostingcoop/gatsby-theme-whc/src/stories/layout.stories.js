import React from "react"
import { graphql } from "gatsby"
import Layout from '../components/layout'

export default {
  title: "Dashboard/layout",
}

export const layoutStory = () => (
  <div>
    <Layout theme="white">
    </Layout>
  </div>
)
